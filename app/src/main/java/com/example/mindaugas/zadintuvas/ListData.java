package com.example.mindaugas.zadintuvas;

/**
 * Created by Mindaugas on 2015-11-07.
 */
public class ListData {

    String Description;
    String title;
    int imgResId;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }

}
