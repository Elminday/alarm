package com.example.mindaugas.zadintuvas;

import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.codetroopers.betterpickers.recurrencepicker.EventRecurrence;
import com.codetroopers.betterpickers.recurrencepicker.EventRecurrenceFormatter;
import com.codetroopers.betterpickers.recurrencepicker.RecurrencePickerDialogFragment;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.codetroopers.betterpickers.recurrencepicker.EventRecurrence;

import org.joda.time.DateTime;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends android.support.v4.app.FragmentActivity implements RadialTimePickerDialogFragment.OnTimeSetListener, RecurrencePickerDialogFragment.OnRecurrenceSetListener {

    ListView lvDetail;
    Context context = MainActivity.this;
    ArrayList<ListData> myList = new ArrayList<ListData>();

    String title = "Žadintuvas";

    String[] desc = new String[1];


    private int j;
    private static final String FRAG_TAG_RECUR_PICKER = "recurrencePickerDialogFragment";
    private EventRecurrence mEventRecurrence = new EventRecurrence();
    private String mRrule;
    private ImageButton setAlarm;
    private ImageButton chooseMusic;
    private ImageButton chooseOptions;
    public int hour, min;
    private TextView text;
    public int interval;
    private Button button;
    String[] alarms = new String[20];
    private int i;
    public MyBaseAdapter adapter;

    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvDetail = (ListView) findViewById(R.id.listas);
        setAlarm = (ImageButton) findViewById(R.id.set_alaarm);
        chooseMusic = (ImageButton) findViewById(R.id.set_music);
        chooseOptions = (ImageButton) findViewById(R.id.set_options);



        adapter = new MyBaseAdapter(context);
        lvDetail.setAdapter(adapter);


        setAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime now = DateTime.now();

                RadialTimePickerDialogFragment
                        timePickerDialog = RadialTimePickerDialogFragment.newInstance(MainActivity.this , now.getHourOfDay(), now.getMinuteOfHour(),
                        DateFormat.is24HourFormat(MainActivity.this));

                timePickerDialog.show(getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);
                Log.d("variables", "lalala");

            }
        });
        chooseMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MusicPopup.show(MainActivity.this);
            }
        });
        chooseOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                Bundle b = new Bundle();
                android.text.format.Time t = new android.text.format.Time();
                t.setToNow();

                b.putLong(RecurrencePickerDialogFragment.BUNDLE_START_TIME_MILLIS, t.toMillis(false));
                b.putString(RecurrencePickerDialogFragment.BUNDLE_TIME_ZONE, t.timezone);

                b.putString(RecurrencePickerDialogFragment.BUNDLE_RRULE, mRrule);

                RecurrencePickerDialogFragment rpd = (RecurrencePickerDialogFragment) fm.findFragmentByTag(
                        FRAG_TAG_RECUR_PICKER);
                if (rpd != null) {
                    rpd.dismiss();
                }
                rpd = new RecurrencePickerDialogFragment();
                rpd.setArguments(b);
                rpd.setOnRecurrenceSetListener(MainActivity.this);
                rpd.show(fm, FRAG_TAG_RECUR_PICKER);

            }
        });
    }
    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        hour = hourOfDay;
        min = minute;
        Log.d("variables", String.valueOf(hour));


        scheduleAlarm();
    }
    public void scheduleAlarm(){
        interval = 1000*60*1;
        Intent intent = new Intent(getApplicationContext(), MyAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, MyAlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        DateTime now = DateTime.now();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), interval, pIntent);

        desc[j] = hour + ":" +min;
        expand();
        j++;

        getDataInList();
    }
    public void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), MyAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, MyAlarmReceiver.REQUEST_CODE, intent,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }
    private void getDataInList() {
        ArrayList<ListData> listas = new ArrayList<ListData>();
        for (int i = 0; i < desc.length-1; i++) {
            // Create a new object for each list item

            ListData ld = new ListData();
            ld.setTitle(title);
            ld.setDescription(desc[i]);
            //ld.setImgResId(img[i]);
            // Add this object into the ArrayList myList
            listas.add(ld);

            //adapter.addItem(ld);
        }
        adapter.setList(listas);
        adapter.notifyDataSetChanged();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public void expand() {
        String[] newArray = new String[desc.length + 1];
        System.arraycopy(desc, 0, newArray, 0, desc.length);

        //an alternative to using System.arraycopy would be a for-loop:
        // for(int i = 0; i < OrigArray.length; i++)
        //     newArray[i] = OrigArray[i];
        desc = newArray;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        // Example of reattaching to the fragment
        super.onResume();
        RadialTimePickerDialogFragment rpd = (RadialTimePickerDialogFragment) getSupportFragmentManager().findFragmentByTag(
                FRAG_TAG_TIME_PICKER);
        if (rpd != null) {
            rpd.setOnTimeSetListener((RadialTimePickerDialogFragment.OnTimeSetListener) this);

        }
    }

    @Override
    public void onRecurrenceSet(String rrule) {
        mRrule = rrule;
        if(mRrule != null){
            mEventRecurrence.parse(mRrule);
        }
        populateRepeats();
    }

    private void populateRepeats() {
        Resources r = getResources();
        String repeatString = "";
        boolean enabled;
        if (!TextUtils.isEmpty(mRrule)) {
            repeatString = EventRecurrenceFormatter.getRepeatString(this, r, mEventRecurrence, true);

        }
        text.setText(mRrule + "\n" + repeatString);

    }
}