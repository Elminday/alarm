package com.example.mindaugas.zadintuvas;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import com.codetroopers.betterpickers.datepicker.DatePickerBuilder;

/**
 * Created by Mindaugas on 2015-10-07.
 */
public class MusicPopup {
    private static PopupWindow popupWindow;

    public static void show(Activity activity) {


        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.music, null);
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        View main = view.findViewById(R.id.main_music);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        popupWindow = new PopupWindow(view, width, height, true);
        popupWindow.showAtLocation(view, Gravity.FILL, 0, 0);
    }
}
